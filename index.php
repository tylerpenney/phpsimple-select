 <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" ></script>
</head>
 
<body>
    <div class="container">
            <div class="row">
                <h3>Customers </h3>
            </div>
            <div class="row">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Customer Number</th>
                      <th>Customer Name</th>
                      <th>total Spent</th></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                   include 'database.php';
                   $pdo = Database::connect();
                   $sql = 'SELECT customers.customerNumber, customerName, SUM(amount) AS totalSpent 
                           FROM payments LEFT JOIN customers ON payments.customerNumber=customers.customerNumber 
                           GROUP BY customers.customerNumber DESC ORDER BY totalSpent LIMIT 5;';
                   $grand_total = 0;
                   foreach ($pdo->query($sql) as $row) {
                            $grand_total += $row['totalSpent'];
                            echo '<tr>';
                            echo '<td>'. $row['customerNumber'] . '</td>';
                            echo '<td>'. $row['customerName'] . '</td>';
                            echo '<td>'. $row['totalSpent'] . '</td>';
                            echo '</tr>';
                   }
                   Database::disconnect();
                  ?>
                  </tbody>
            </table>
            
        </div>
    </div> <!-- /container -->
  </body>
</html>